## Installing app
- `npm install --save --legacy-peer-deps`

## Run app
- `npm run dev`
- application is running on `http://localhost:3002/`

## Notes
- pre jednoduchsi vyvoj som aplikaciu vytvaral z vlastneho 'boilerplate'
- vyuzival som Bootstrap, Fontawessome - neskor som zakomponoval do niektorych casti aj https://mui.com/, ktore bolo v zadani
- v MovieSearch som vyuzil komponentu InputText - tuto komponentu som uz mal vytvorenu z predchadzajuceho projektu, preto som ju pouzil

## Knowing issues
- pri vyhľadaní filmu a následného zobrazení podrobností o filme, pri vrátení sa v histórii browsera späť sa nezobrazia pôvodné výsledky vyhľadávania
- pri vyhľadávaní nie je tlačítko ako bolo v zadaní - vynechal som to pre krátkosť času


