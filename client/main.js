"use strict"

import React from 'react';
import Header from './components/header';

class Main extends React.Component{
    render(){
        return(
            <div>
                <Header />

                <main className="page-content">
                    <div className="container">
                        {this.props.children}
                    </div>

                </main>

            </div>
        );
    }
}



export default Main;