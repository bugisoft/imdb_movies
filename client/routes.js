"use strict"

import React from 'react';

import {Router, Route, IndexRoute, browserHistory} from 'react-router';
import MovieSearch from './components/pages/movieSearch';
import MovieDetail from './components/pages/movieDetail';
import FavouriteMovies from './components/pages/favouriteMovies';
import Main from './main';

const routes = (
    <Router history={browserHistory}>
        <Route path="/" component={Main}>
            <IndexRoute component={MovieSearch}/>
            <Route path="/movie/:id" component={MovieDetail}/>
            <Route path="/favourite-movies" component={FavouriteMovies}/>
        </Route>
    </Router>
);

export default routes;