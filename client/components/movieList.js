"use strict"

import React from 'react';
import MoviePreview from './moviePreview';
import {connect} from 'react-redux';


class MovieList extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        let postsList;
        if( this.props.movies != undefined ) {
            postsList = this.props.movies.map(function (postsArr) {
                return (
                    <div className="col" key={postsArr._id}>
                        <MoviePreview
                            _id={postsArr._id}
                            imdbID={postsArr.imdbID}
                            title={postsArr.Title}
                            year={postsArr.Year}
                            image={postsArr.Poster}
                        />
                    </div>
                )
            })
        }


        return(
                
                <div className="row col-five">

                    {postsList}

                </div>
    
        );
    }
}

function mapStateToProps(state){
    return{
        movies: state.movies.movies
    }
}

export default connect(mapStateToProps, null)(MovieList);