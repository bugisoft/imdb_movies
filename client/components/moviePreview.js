"use strict"

import React from 'react';
import {Link} from "react-router";

class MoviePreview extends React.Component{

    constructor(){
        super();
    }

    render(){

        return(
            <span>
                <Link to={`/movie/` + this.props.imdbID}>
                    <div className="cart-movie">
                        <img src= { this.props.image } alt=""/>
                        <h2>{this.props.title}</h2>
                        <div className='year'>{this.props.year}</div>
                    </div>
                </Link>
            </span>
        );
    }
}

export default MoviePreview;