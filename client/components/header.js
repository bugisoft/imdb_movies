"use strict"

import React from 'react';
import { Link } from 'react-router';

class Header extends React.Component{
    render(){
        return(
            <header className="page-header">
                <div className="container">
                    <div className="row">
                        <div className="col-md-3">
                            <Link to={`/`}>
                                <div className="logo">
                                    IMDB movies
                                </div>
                            </Link>
                        </div>
                        <div className="col-md-9">
                            <nav className='menu-main'>
                                <ul>
                                    <li><Link to={`/favourite-movies`}>Favourite movies</Link></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>


            </header>
        );
    }
}

export default Header;