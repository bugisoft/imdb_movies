"use strict"

import React from 'react';
import InputText from "../form/formParts/inputText";
import MovieList from "../movieList";
import Formsy from "formsy-react";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getMovies, flushMovies} from '../../actions/moviesActions';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';

class MovieSearch extends React.Component{
    constructor(){
        super();
        
        this.state = {
            searchInput : '',
            searchResults : []
        }

        this.setSearchInput = this.setSearchInput.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.loadMoreMoviesClick = this.loadMoreMoviesClick.bind(this);

    }

    componentDidMount(){
        this.props.flushMovies();
    }

    setSearchInput( searchInput ){
        this.setState({ searchInput: searchInput });
    }

    submitForm(model) {

        let searchInput = this.state.searchInput;

        this.props.getMovies( searchInput, 1 );
    }

    loadMoreMoviesClick(){
        let page = this.props.movies.length / 10 + 1;

        this.props.getMovies( this.state.searchInput, page );
    }

    invalidSubmitForm(){
        console.log('invalid submit');
    }

    render(){

        let loadMoreButton;

        if( this.props.movies.length != 0 && ( this.props.totalResults > this.props.movies.length )){
            loadMoreButton = (
                <Box textAlign='center'>
                    <Button variant="contained" color="primary" size="large" onClick={ this.loadMoreMoviesClick }>Load more movies</Button>
                </Box>
            )
        }

        let searchMessage;
        if( this.props.totalResults == 0 ){
            searchMessage = (
                <Box textAlign='center'>
                    <p>
                        We can't find any result for your search query.
                    </p>
                </Box>
            )
        }

        return(
            <div className='page-movie-search'>
                <Formsy onValidSubmit={this.submitForm} onInvalidSubmit={this.invalidSubmitForm}>
                    <InputText
                        label = 'Movie search'
                        name = 'postURL'
                        value = {this.state.searchInput}
                        setParentValue = {this.setSearchInput}
                        placeholder = 'e.g.: Batman'
                        showErrorAfterBlur = {true}
                        validations="isExisty"
                        validationError="Please insert search string"
                        required
                    />
                </Formsy>

                { searchMessage }

                <MovieList/>

                { loadMoreButton }


            </div>
        )
    }
}

function mapStateToProps(state){
    return{
        movies: state.movies.movies,
        totalResults: state.movies.totalResults
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getMovies, flushMovies}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(MovieSearch);
