"use strict"

import React from 'react';
import MovieList from "../movieList";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getFavouriteMovies} from '../../actions/moviesActions';


class FavouriteMovies extends React.Component{
    constructor(){
        super();
    }

    componentDidMount(){
        this.props.getFavouriteMovies();
    }

    render(){

        return(
            <span>

                <MovieList/>

            </span>
        )
    }
}


function mapDispatchToProps(dispatch){
    return bindActionCreators({ getFavouriteMovies }, dispatch)
}
export default connect(null, mapDispatchToProps)(FavouriteMovies);
