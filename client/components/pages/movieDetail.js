"use strict"

import React from 'react';
import axios from "axios";
import Grid from '@mui/material/Grid';

class MovieDetail extends React.Component{
    constructor(){
        super();

        this.state = {
            movieData : '',
            favouriteMovie: false
        }

        this.setFavourite = this.setFavourite.bind(this);
        this.initFavourite = this.initFavourite.bind(this);

    }

    componentDidMount(){
        this.setMovie();
        this.initFavourite();
    }

    initFavourite(){
        let movieID = this.props.params.id;
        let favouriteMovies = JSON.parse( localStorage.getItem('favouriteMovies') );

        if( favouriteMovies != undefined && this.isInArray(favouriteMovies, movieID) ) this.setState({ favouriteMovie: true });
    }

    setMovie(){
        let movieID = this.props.params.id;

        axios.get("http://omdbapi.com/?apikey=73f7bb03&i=" + movieID).then( (res) => {

            if( res.data.Response == 'True') {

                this.setState({movieData: res.data});

            }
            else{
                console.log('Žiadne výsledky vyhľadávania');
            }

        }).catch( (error) => {
            console.log(error);
        })
    }

    setFavourite(){

        let favouriteMovies = ( JSON.parse( localStorage.getItem('favouriteMovies') ) || [] );

        let movie = this.state.movieData;

        if( this.state.favouriteMovie ){
            favouriteMovies = this.removeMovie( favouriteMovies, movie);
        }
        else{
            favouriteMovies.push( movie );
        }

        localStorage.setItem('favouriteMovies', JSON.stringify(favouriteMovies));

        this.setState({ favouriteMovie: !this.state.favouriteMovie });

    }

    isInArray( array, movieID ){
        for( let arrayObject of array){
            if(arrayObject.imdbID == movieID) return true;
        }

        return false;
    }

    removeMovie( favouriteMovies, movie ){
        let newFavouriteMovies = [];
        for( let favouriteMovie of favouriteMovies){
            if(favouriteMovie.imdbID != movie.imdbID) newFavouriteMovies.push( favouriteMovie );
        }

        return newFavouriteMovies;
    }



    render(){

        let movie = this.state.movieData;

        let favouriteActive = '';
        if( this.state.favouriteMovie ) favouriteActive = 'active';

        return(
            <div className='page-movie-detail'>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <img src={movie.Poster} alt=""/>
                    </Grid>
                    <Grid item xs={8}>
                        <Grid container spacing={2}>
                            <Grid item xs={11}>
                                <h1> { movie.Title } </h1>
                            </Grid>
                            <Grid item xs={1}>
                                <i className={'fa fa-star ' + favouriteActive} onClick={this.setFavourite}></i>
                            </Grid>
                        </Grid>

                        <p>
                            <strong>Year:</strong> { movie.Year } <br/>
                            <strong>Director:</strong> { movie.Director } <br/>
                            <strong>Country:</strong> { movie.Country } <br/>
                            <strong>Runtime:</strong> { movie.Runtime } <br/>
                        </p>

                        <p className='plot'>
                            { movie.Plot }
                        </p>


                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default MovieDetail;
