"use strict"

import React from 'react';
import { withFormsy } from 'formsy-react';


class InputText extends React.Component{
    constructor(props) {
        super(props);
        
        this.state = { 
            onBlur : false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleOnBlur = this.handleOnBlur.bind(this);

    }
    handleChange(event) {
        
        this.props.setParentValue(event.target.value);
        
        this.props.setValue(event.currentTarget.value);
    }

    handleOnBlur(event){
        this.setState({ onBlur : true });
    }

    render(){

        let errorMessage = '';
        let validation = '';
        if(this.props.isPristine() == false){ 

            if(this.props.showRequired()) errorMessage += 'This field is required.';

            if(errorMessage != '') validation = ' is-invalid';
        }


        return(
            <div className="form-group">
                <label>{this.props.label}</label>

                <input 
                    type = "text" 
                    name = {this.props.name}
                    className = {"form-control" + validation}
                    placeholder = {this.props.placeholder} 
                    value={this.props.getValue() || ''} 
                    onChange = {this.handleChange}
                    onBlur = {this.handleOnBlur} 
                />
                <div className="invalid-feedback">{errorMessage}</div>
            </div>
        )
    }
}

export default withFormsy(InputText);
