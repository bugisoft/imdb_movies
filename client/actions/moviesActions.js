"use strict"
import axios from 'axios';

export function getMovies( searchInput, page ){
    return function(dispatch){

        axios.get("http://omdbapi.com/?apikey=73f7bb03&s=" + searchInput + "&page=" + page).then( (res) => {

            if( res.data.Response == 'True'){
                console.log(res.data);

                if(page == 1) dispatch({type:"GET_MOVIES", payload: res.data});
                else dispatch({type:"APPEND_MOVIES", payload: res.data})
            }
            else{

                dispatch({type:"ZERO_RESULTS", payload: 0});
            }

            //this.props.setSubmitLoader(false);

        }).catch( (err) => {
            dispatch({type:"GET_MOVIES_REJECTED", payload:err})
        })

    }
}

export function getFavouriteMovies(){
    return function(dispatch){

        let favouriteMovies;
        favouriteMovies = {
            Search : JSON.parse( localStorage.getItem('favouriteMovies') )
        }

        console.log( JSON.parse( localStorage.getItem('favouriteMovies')) );

        dispatch({type:"GET_MOVIES", payload: favouriteMovies});

    }
}

export function flushMovies(){
    return function(dispatch){
        dispatch({type:"FLUSH_MOVIES", payload:''});
    }
}