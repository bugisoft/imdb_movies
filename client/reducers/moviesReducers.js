"use strict"

export function moviesReducers(state={ movies: [] }, action){
    switch(action.type){
        case "GET_MOVIES":
            return {
                ...state,
                movies: [...action.payload.Search],
                totalResults: action.payload.totalResults
            }
            break;
        case "ZERO_RESULTS":
            return {
                ...state,
                movies: [],
                totalResults: action.payload
            }
            break;
        case "APPEND_MOVIES":
            return {
                ...state,
                movies: state.movies.concat(action.payload.Search),
                totalResults: action.payload.totalResults
            }
            break;
        case "FLUSH_MOVIES":
            return {
                ...state,
                movies:[...action.payload]
            }
            break;
    }
    return state
}