import * as cluster from "./cluster";
import * as coordinates from "./coordinates";
import * as array from "../_lib/array";
import * as type from "./type";
import * as country from "./country";
import * as glEntity from "./geoLocationEntity";
import axios from "axios";

class GeoLocationParser {

    constructor() {
        this.namedEntities = [];
    }

    async getGeoEntities(namedEntities) {

        namedEntities = await this.setGeoLocations(namedEntities);

        namedEntities = country.removeCountries(namedEntities);

        namedEntities = glEntity.groupEntitiesByGeoLocation(namedEntities);

        let nonFilteredTypesNamedEntities = namedEntities.slice();
        namedEntities = type.removeNotRelevantTypes(namedEntities);

        namedEntities = glEntity.removeEntitiesOfHugeArea(namedEntities);

        let clusters = cluster.getClusters(namedEntities);

        //console.log('--clusters start ' + JSON.stringify(clusters, null, 10) + ' --clusters end');

        let mostRelevantCluster = cluster.getMostRelevantCluster(clusters);

        if( mostRelevantCluster == undefined) throw 'noTravelArticle';

        //console.log(mostRelevantCluster);

        let coordinatesCenter = coordinates.getCenterOfCoordinates(mostRelevantCluster.namedEntities);
        let bounds = coordinates.getBounds(coordinatesCenter);

        mostRelevantCluster = await glEntity.addMostRelevantNamedEntities(mostRelevantCluster, nonFilteredTypesNamedEntities, coordinatesCenter, bounds);

        mostRelevantCluster.namedEntities = array.orderByRelevancy(mostRelevantCluster.namedEntities);

        this.namedEntities = mostRelevantCluster.namedEntities;

        //console.log('--mostRelevantCluster start' + JSON.stringify(mostRelevantCluster, null, 10) + ' --mostRelevantCluster end');

        namedEntities = this.getMostRelevantNamedEntities( mostRelevantCluster.namedEntities );

        return namedEntities;

    }

    async setGeoLocations(namedEntities) {
        let newNamedEntities = [];
        let geocodeRequests = [];

        for (let namedEntity of namedEntities) {

            let urlRequest = "https://maps.googleapis.com/maps/api/geocode/json?address=" + encodeURI(namedEntity.word) + "&key=AIzaSyBfqxNNsuqehBwQwy_aShjUZ9mEg13g4mQ&language=sk";

            geocodeRequests.push( axios.get(urlRequest) );

        }


        let promiseResponses = await Promise.allSettled(geocodeRequests);
        let geocodeResponses = await this.getGeocodeResponses( promiseResponses );


        for (let [index, geocodeResponse] of geocodeResponses.entries()) {

            let namedEntity = glEntity.setGeoLocation(geocodeResponse, namedEntities[index]);
            if (namedEntity != '') newNamedEntities.push(namedEntity);
        }

        return newNamedEntities;
    }

    async getGeocodeResponses(promiseResponses ){
        let geocodeResponses = [];

        for(let promiseResponse of promiseResponses){
            if( promiseResponse.status == 'fulfilled' ) geocodeResponses.push(promiseResponse.value.data);
            else{

                let urlRequest = promiseResponse.reason.config.url;
                let newResponse;

                console.log('-----REJECTED----- ' + urlRequest);

                try {
                    newResponse = await axios.get( urlRequest );
                }
                catch(error){
                    console.log(error);
                }

                console.log('isiel som dalej');
                if( newResponse != undefined ) geocodeResponses.push( newResponse.data );
                else geocodeResponses.push( { results: [] } );

            }
        }

        return geocodeResponses;
    }

    getMostRelevantNamedEntities(namedEntities) {
        let mostRelevantNE = [];
        let count = 0;

        for (let nameEntity of namedEntities) {
            if (nameEntity.types.includes('administrative_area_level_1')) continue;
            else {
                mostRelevantNE.push(nameEntity);
                count++;
            }

            if (count == 3) break;
        }

        return mostRelevantNE;
    }

    getNamedEntities(){
        return this.namedEntities;
    }
}

export default GeoLocationParser;