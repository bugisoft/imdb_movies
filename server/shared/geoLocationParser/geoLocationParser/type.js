export function removeNotRelevantTypes( namedEntities ){
    let filteredNamedEntities = [];

    for( let namedEntity of namedEntities ){
        if( isRelevantType( namedEntity ) ) filteredNamedEntities.push( namedEntity );
    }

    return filteredNamedEntities;
}

export function isFilteredType( result ){
    if( result.types.includes('country') || result.types.includes('continent') ){ // dal som prec || result.types.includes('administrative_area_level_1')
        console.log('---PRESKOCENE: ' + result.address_components[0].long_name);
        return true;
    }
    else return false
}

export function isRelevantType( namedEntity ){
    let nonRelevantTypes = [ 'route', 'food', 'restaurant', 'cafe', 'bar', 'doctor', 'health', 'store', 'bank' ];

    for( let nonRelevantType of nonRelevantTypes){
        if( namedEntity.types.includes( nonRelevantType ) ) return false;
    }

    return true;
}