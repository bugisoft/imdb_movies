export function getCenterOfCoordinates(arr){
    let minX, maxX, minY, maxY;
    for (let i = 0; i < arr.length; i++){
        minX = (arr[i].location.lat < minX || minX == null) ? arr[i].location.lat : minX;
        maxX = (arr[i].location.lat > maxX || maxX == null) ? arr[i].location.lat : maxX;
        minY = (arr[i].location.lng < minY || minY == null) ? arr[i].location.lng : minY;
        maxY = (arr[i].location.lng > maxY || maxY == null) ? arr[i].location.lng : maxY;
    }
    return [(minX + maxX) / 2, (minY + maxY) / 2];
}

export function getBounds( center ){
    let bounds = [{},{}];

    bounds[0].lat = center[0] - 0.4;
    bounds[0].lng = center[1] - 0.4;
    bounds[1].lat = center[0] + 0.4;
    bounds[1].lng = center[1] + 0.4;

    if( bounds[0].lat > 180 || bounds[0].lat < -180) bounds[1].lat = center[0];
    if( bounds[0].lng > 180 || bounds[0].lng < -180) bounds[1].lng = center[1];
    if( bounds[1].lat > 180 || bounds[1].lat < -180) bounds[1].lat = center[0];
    if( bounds[1].lng > 180 || bounds[1].lng < -180) bounds[1].lng = center[1];

    return bounds;

}

export function calcDistance(lat1, lon1, lat2, lon2) {
    var R = 6371; // km
    var dLat = toRad(lat2-lat1);
    var dLon = toRad(lon2-lon1);
    var lat1 = toRad(lat1);
    var lat2 = toRad(lat2);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return d;
}

// Converts numeric degrees to radians
function toRad(Value){
    return Value * Math.PI / 180;
}