import * as string from "../_lib/string";
import * as array from '../_lib/array';

export function setCountries( result ){
    let countries = [];

    for( let address of result.address_components){
        if( address.types.includes('country') ){ // vymazal som || address.types.includes('administrative_area_level_1') pri clankoch ako https://ourway.sk/blog/malajzia/vyletik-okolo-malajzie/ to robilo sarapatu
            countries.push( address.long_name );
        }
    }

    return countries;
}

/*
    Funkcia vymaze krajiny, to ci je slovo krajina "usvedcuju" namedEntities.

    Preco funkcia removeCountries() nema byt vymazana a nahradena removeEntitiesOfHugeArea():
    Funkcia removeEntitiesOfHugeArea() je podobna, ale rozdiel je v tom, ze krajiny sa vymazu (pretoze su usvedcene) pred funkciou addMostRelevantNamedEntities().
    Entity, ktore sa vymazu v removeEntitiesOfHugeArea() nemusia byt urcene spravne - mozno by sa nemali vymazat.

    Priklad: Slovensko sa vymaze, keby sa nevymazalo, tak pri lokalite Slovenské Nové Mesto by mohlo zle urcit polohu

    Keby sa napr. Vymazali Alpy, tak napr. mesto Alpis by sa uplne stratilo

    TODO: krajinu s malou rozlohou nevymazat
 */

export function removeCountries( namedEntities ){
    let countries = getMostRelevantCountries( namedEntities );

    for( let [ index, namedEntity ] of namedEntities.entries() ){
        for ( let country of countries ){
            let similarity = string.similarityOfStrings(country, namedEntity.word);
            if (similarity > 0.75){
                console.log('---Vymazavam')
                console.log(namedEntity);
                namedEntities[index] = undefined;
                break;
            }
        }
    }

    namedEntities = array.removeUndefinedItems( namedEntities );

    return namedEntities;
}

function getMostRelevantCountries( namedEntities ){
    let countries = [];

    for( let namedEntity of namedEntities ){
        if( namedEntity.relevancy <= 2 ) return countries;
        for( let country of namedEntity.country ){
            if( !countries.includes(country) ) countries.push( country );
        }
    }

    return countries;
}