import * as array from "../_lib/array";
import * as coordinates from "./coordinates";

export function getClusters(namedEntities) {
    let clusters = calculateClusters( namedEntities );

    clusters = setClusterRelevancy(clusters);

    clusters = array.orderByRelevancy( clusters );

    return clusters;
}

function calculateClusters(namedEntities) {
    let clusters = [];

    for( let namedEntity of namedEntities){
        let namedEntityAdded = false;

        for( let cluster of clusters){

            for( let clusterNamedEntity of cluster.namedEntities){

                let distance = coordinates.calcDistance(namedEntity.location.lat,  namedEntity.location.lng, clusterNamedEntity.location.lat, clusterNamedEntity.location.lng );

                if (distance <= 120) {  // zmenil som z 80 na 200

                    cluster.namedEntities.push(namedEntity);
                    namedEntityAdded = true;
                    break;

                }

            }
            if( namedEntityAdded == true ) break;
        }

        if(namedEntityAdded == false) clusters.push( {'namedEntities': [namedEntity]} );

    }

    return clusters;
}

export function setClusterRelevancy(clusters) {

    for(let cluster of clusters ){
        let relevancy = getClusterRelevancy( cluster );
        cluster.relevancy = relevancy;
    }

    return clusters;
}

function getClusterRelevancy(cluster){
    let relevancy = 0;
    for( let namedEntity of cluster.namedEntities){
        relevancy += namedEntity.relevancy;
    }
    return relevancy;
}

export function getMostRelevantCluster( clusters ){
    for(let cluster of clusters){
        if( cluster.namedEntities.length > 1 ) return cluster;
    }

    //if( clusters.length > 0 && clusters[0].namedEntities.length > 1) return clusters[0]; // toto bola povodna verzia, ale aj cestovatelske clanky hodnotila ako necestovatelske!
    if( clusters.length > 0 ) return clusters[0];
}