import axios from "axios";
import * as array from '../_lib/array';
import * as string from '../_lib/string';
import * as type from "./type";
import * as country from "./country";
import * as coordinates from './coordinates';


export function setGeoLocation( res, namedEntity ){
    let result = res.results[0];

    console.log(namedEntity.word);
    if(result != undefined) console.log(res.results[0].formatted_address);

    if(result != undefined && !type.isFilteredType( result )) {



        let location = result.geometry.location;

        let name = getGeoLocationName( result );
        let nameSimilarity = string.similarityOfStrings( name, namedEntity.word );
        if( name != '' && nameSimilarity >= 0.75 ) namedEntity.word = name; // z 0.75 som zmenil na 0.4 - Titicacy -> jazero Titicaca

        let countries = country.setCountries( result );

        namedEntity = {
            word: namedEntity.word,
            lemma: namedEntity.lemma,
            relevancy: namedEntity.relevancy,
            location: {lat: location.lat, lng: location.lng},
            locationString: location.lat + '' + location.lng,
            bounds: result.geometry.bounds,
            types: result.types,
            country: countries
        };

        return namedEntity;
    }
    else return '';
}


function getGeoLocationName( result ){
    let name = '';
    for( let address of result.address_components){
        if(address.types.includes('street_number') || address.types.includes('route')) continue;
        else{
            name = address.short_name;
            return name;
        }
    }

    return name;
}

export function groupEntitiesByGeoLocation( namedEntities ){

    namedEntities = countNamedEntities( namedEntities );
    namedEntities = array.orderByRelevancy( namedEntities );
    return namedEntities;
}

export function countNamedEntities( namedEntities ){
    let countedNameEntities = [];

    for (let nameEntity of namedEntities) {
        let findedNamedEntity = array.isInArray( countedNameEntities, nameEntity, 'locationString' );

        if( findedNamedEntity == null ) countedNameEntities.push( nameEntity );
        else findedNamedEntity = increaseRelevancy( findedNamedEntity, nameEntity );

    }

    return countedNameEntities;

}

function increaseRelevancy(findedNamedEntity, nameEntity){  //TODO: trebalo by otestovat, ci po pridani funkcie do namedEntity Parsera namedEntity.groupEntitiesBySimilarity() este ma zmysel tato funkcia - tipujem, ze pri malom pocte pripadov asi ano

    findedNamedEntity.relevancy += nameEntity.relevancy;
    if( findedNamedEntity.groupedWords == undefined ) findedNamedEntity.groupedWords = nameEntity.word;
    else findedNamedEntity.groupedWords += ', ' + nameEntity.word;

    return findedNamedEntity;

}

export async function addMostRelevantNamedEntities( mostRelevantCluster, namedEntities, center, bounds ) {
    let geocodeRequests = [];

    for ( let i=0; i < namedEntities.length; i++ ){

        let findedNamedEntity = array.isInArray( mostRelevantCluster.namedEntities, namedEntities[i], 'locationString' );

        if( findedNamedEntity == null ) {

            let urlRequest = "https://maps.googleapis.com/maps/api/geocode/json?address=" + encodeURI(namedEntities[i].word) + "&key=AIzaSyBfqxNNsuqehBwQwy_aShjUZ9mEg13g4mQ&language=sk";
            urlRequest += '&bounds=' + bounds[0].lat + ',' + bounds[0].lng + '|' + bounds[1].lat + ',' + bounds[1].lng;

            console.log('addMostRelevantNamedEntities ' + urlRequest);

            geocodeRequests.push(axios.get(urlRequest));
        }

        if( i == 2 ) return mostRelevantCluster;
    }

    let geocodeResponses = await Promise.all(geocodeRequests);

    for( let [ index, geocodeResponse ] of geocodeResponses.entries() ){

        let namedEntity = setGeoLocation( geocodeResponse, namedEntities[index] );
        if( namedEntity.location != undefined && type.isRelevantType( namedEntity )) {

            let distance = coordinates.calcDistance(namedEntity.location.lat, namedEntity.location.lng, center[0], center[1]);

            if (distance <= 120) mostRelevantCluster.namedEntities.push(namedEntity);
        }


    }

    return mostRelevantCluster;
}

export function removeEntitiesOfHugeArea( namedEntities ){
    let newNamedEntities = [];
    for (let namedEntity of namedEntities){

        if( namedEntity.bounds != undefined ) {
            let coordinate1 = namedEntity.bounds.northeast;
            let coordinate2 = namedEntity.bounds.southwest;
            let distance = coordinates.calcDistance(coordinate1.lat, coordinate1.lng, coordinate2.lat, coordinate2.lng);

            if (distance > 150) continue;
        }

        newNamedEntities.push( namedEntity );
    }

    return newNamedEntities;

}