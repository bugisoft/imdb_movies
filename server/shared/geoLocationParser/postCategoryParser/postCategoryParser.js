import * as array from "../_lib/array";

export function getPostCategory( namedEntities, processedWords ){

    let postCategoryGroups = [
            {
                group: 'natural_feature',
                types: ['natural_feature', 'park'],  // pridal som 'park' - napriklad Pacaya Samiria je park a nie natural_feature, takisto aj napr. Yellowstone
                keywords: [],
                relevancy: 0
            },
            {
                group: 'locality',
                types: ['locality'],
                keywords: [],
                relevancy: 0
            },
            {
                group: 'city',
                types: ['art_gallery',  'museum', 'park', 'stadium', 'aquarium', 'spa'],
                keywords: ['múzeum', 'múzea', 'múzeu', 'múzeom', 'múzeá', 'múzeám', 'múzeách', 'múzeami', 'námestie', 'námestia','námestiu','námestí','námestím','námestiam','námestiach','námestiami'], // TODO: pridat 'park', ale urobit vylucovanie s pojmom 'narodny park', otestovat na https://ourway.sk/blog/peru/uchopitelne-dobrodruzstvo-amazonas/
                findedKeywords: [],
                relevancy: 0
            },
            {
                group: 'culture',
                types: ['church', 'mosque', 'synagogue'],
                keywords: ['kostol','kostolu','kostole','kostolom','kostoly','kostolov','kostoloch','kostolami','mešit*', 'synagóg*', 'hrad*', 'zámok', 'zrúcanin*'],
                findedKeywords: [],
                relevancy: 0
            },
            {
                group: 'attraction',  // ako atrakcia moze byt napr. Mitad del mundo
                types: ['tourist_attraction', 'point_of_interest'],
                keywords: [],
                findedKeywords: [],
                relevancy: 0
            },
            {
                group: 'bicycle',
                types: [],
                keywords: ['bicyk*', 'cyklopis*', 'cyklovačk*'],
                findedKeywords: [],
                relevancy: 0
            },
        {
            group: 'winter-sport',
            types: [],
            keywords: ['lyže', 'lyží', 'lyžiam', 'lyžiach', 'lyžami', 'lyžova*', 'bežky', 'bežkách', 'bežkami', 'bežiek', 'bežkova*', 'skialp*' ], // pridal som skialp* a vyhodil 'skialpy', 'skialpové', 'skialpinistické'
            findedKeywords: [],
            relevancy: 0
        },
        {
            group: 'winter-hiking',
            types: [],
            keywords: ['sneh*', 'sneženie', 'sneženia', 'sneženiu', 'snežení', 'snežením', 'snežiť', 'snežilo',  'cencúľ', 'cencúle', 'ľad', 'ľadu', 'ľadom','ľady'],
            findedKeywords: [],
            relevancy: 0
        },
    ];

    namedEntities = removeNotRelevantTypes( namedEntities );

    postCategoryGroups = getTypesRelevancy( namedEntities, postCategoryGroups );

    postCategoryGroups = getKeywordsRelevancy( processedWords, postCategoryGroups );

    postCategoryGroups = array.orderByRelevancy( postCategoryGroups );

    //console.log('--postCategoryGroups start' + JSON.stringify(postCategoryGroups, null, 10) + ' --postCategoryGroups end');

    let postCategory = getMostRelevantCategory( postCategoryGroups );


    return postCategory;
}

function removeNotRelevantTypes( namedEntities ){
    for( let namedEntity of namedEntities ){
        let types = namedEntity.types;

        if( types.includes('park')){  // park velmi nie je attraction group ('point_of_interest' alebo 'tourist_attraction'), ale skor 'natural_feature' alebo 'city' group |=> https://ourway.sk/blog/peru/uchopitelne-dobrodruzstvo-amazonas/
            types = array.removeValueFromArray(types, 'point_of_interest');
            types = array.removeValueFromArray(types, 'tourist_attraction');

            namedEntity.types = types;
        }
    }

    return namedEntities;
}

function getTypesRelevancy( namedEntities, postCategoryGroups ) {

    for( let namedEntity of namedEntities ){
        for( let postCategoryGroup of postCategoryGroups ) {
            for (let groupType of postCategoryGroup.types) {
                if (namedEntity.types.includes(groupType)) postCategoryGroup.relevancy += namedEntity.relevancy;
            }
        }
    }

    return postCategoryGroups;
}

function getKeywordsRelevancy( processedWords, postCategoryGroups ){

    for( let postCategoryGroup of postCategoryGroups ) {
        for( let keyword of postCategoryGroup.keywords ) {
            for( let processedWord of processedWords ) {
                if( isWordEqual(processedWord.word, keyword) ){
                    postCategoryGroup.relevancy += processedWord.relevancy;
                    postCategoryGroup.findedKeywords.push( processedWord.word.text );
                }
            }
        }
    }

    return postCategoryGroups;
}

function isWordEqual(processedWord, keyword){
    if( keyword.includes('*') ){
        keyword = keyword.replace('*', '');
        if( processedWord.text.startsWith(keyword) ) return true;
    }
    else if( processedWord.text == keyword ) return true;

    return false;
}

function getMostRelevantCategory( postCategoryGroups ){

    for( let postCategoryGroup of postCategoryGroups ) {

        let bicycleGroup = getGroup(postCategoryGroups, 'bicycle');
        if( bicycleGroup.relevancy > 4 ) return 'bicycle';

        let winterSportGroup = getGroup(postCategoryGroups, 'winter-sport');
        if( winterSportGroup.relevancy > 4 ) return 'winter-sport';

        let winterHiking = getGroup(postCategoryGroups, 'winter-hiking');
        if( winterHiking.relevancy > 4 ) return 'winter-hiking';

        if(postCategoryGroup.group == 'natural_feature'){

            if( winterHiking.relevancy > 4 ){
                if( winterSportGroup.relevancy > winterHiking.relevancy ) return 'winter-sport';
                else return 'winter-hiking';
            }
            else return 'hiking';
        }

        if(postCategoryGroup.group == 'locality') return 'adventure';

        if(postCategoryGroup.group == 'city') return 'city';

        if(postCategoryGroup.group == 'culture') return 'culture';

        if(postCategoryGroup.group == 'attraction') return 'attractions';

        return 'adventure';
    }
}

function getGroup( postCategoryGroups, groupName ){
    for( let postCategoryGroup of postCategoryGroups ) {
        if( postCategoryGroup.group == groupName ) return postCategoryGroup;
    }

    return null;
}

