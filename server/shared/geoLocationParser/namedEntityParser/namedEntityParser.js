
import * as cheerio from "cheerio";
import { decode } from 'html-entities';
import * as namedEntity from './namedEntity';
import * as array from '../_lib/array';
import * as nlpParser from "./nlpParser";

class NamedEntityParser {

    constructor() {
        this.potentialNamedEntities = [];
        this.namedEntities = [];
        this.words = [];
        this.processedPosLemmaWords = [];
    }

    async getNamedEntities( articleData ) {

        let stringsForProcessing = this.setStringsForProcessing(articleData);

        await this.processStrings( stringsForProcessing );

        this.namedEntities = namedEntity.addRelevantPotentialEntities( this.namedEntities, this.potentialNamedEntities );

        this.namedEntities = namedEntity.groupEntitiesBySimilarity( this.namedEntities );

        this.namedEntities = namedEntity.countNamedEntities(this.namedEntities);
        this.namedEntities = array.orderByRelevancy(this.namedEntities);

        //console.log('--namedEntities start' + JSON.stringify(this.namedEntities, null, 10) + ' --namedEntities end');

        return this.namedEntities;

    }

    async processStrings( stringsForProcessing ) {

        for (let stringForProcessing of stringsForProcessing) {
            await this.processString (stringForProcessing.string, stringForProcessing.relevancy );
        }

    };

    setStringsForProcessing( articleData ) {
        let stringsForProcessing = [];

        let content = decode( articleData.content );
        let title = decode( articleData.title );
        const $ = cheerio.load(content);

        let stripedHtml = content.replace(/<[^>]+>/g, '.');

        let strongs = $('strong, h2, b');


        stringsForProcessing.push({ string: stripedHtml, relevancy: 1 });

        for (let strong of strongs) {
            stringsForProcessing.push({ string: $(strong).text(), relevancy: 2 });
        }

        stringsForProcessing.push({ string: title, relevancy: 3 });


        return stringsForProcessing;
    }

    async processString( inputString, relevancy ) {

        let words = nlpParser.getWords( inputString );

        let parsedNamedEntities = await namedEntity.getNamedEntities(words, relevancy, this.namedEntities, this.potentialNamedEntities, this.processedPosLemmaWords);

        this.namedEntities = parsedNamedEntities.namedEntities;
        this.potentialNamedEntities = parsedNamedEntities.potentialNamedEntities;

        this.setWords( words, relevancy);

    }

    setWords(words, relevancy ){

        for (let word of words ){
            let newWord = {
                word : {
                    text: word.word.text.toLowerCase(),
                    lemma: word.word.lemma,
                    pos: word.word.pos,
                },
                relevancy: relevancy
            }

            this.words.push( newWord );
        }
    }

    getWords(){
        return this.words;
    }

}

export default NamedEntityParser;



