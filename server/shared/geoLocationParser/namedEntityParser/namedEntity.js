import * as string from '../_lib/string';
import * as array from '../_lib/array';
import * as nlpParser from "./nlpParser";

export async function getNamedEntities( words, relevancy, namedEntities, potentialNamedEntities, processedPosLemmaWords ){

    let sentenceBegining = true;

    for (let index = 0; index < words.length; index++) {

        let word = words[index].word;


        if( string.initialIsCapital( word.text ) ){

            words[index].word = await nlpParser.getPosAndLemma(word.text, processedPosLemmaWords);

            let namedEntity = await getNamedEntity(words, index, sentenceBegining, processedPosLemmaWords);

            if( namedEntity.word != '' && namedEntity.word.length > 3){ //vacsie ako 3 - odfiltruje to skratky ako EUR, SPZ, BB
                if( namedEntity.relevancy == 'named-entity' ){

                    namedEntity.relevancy = relevancy;
                    namedEntities.push( namedEntity );

                }
                else{
                    namedEntity.relevancy = 1;
                    potentialNamedEntities.push( namedEntity );
                }

                let wordCount = namedEntity.word.split(' ').length;
                index += (wordCount - 1);
            }
        }

        if(word.pos == 'PUNCT') sentenceBegining = true;
        else sentenceBegining = false;

    }

    return { namedEntities: namedEntities, potentialNamedEntities: potentialNamedEntities };
}



async function getNamedEntity(words, index, sentenceBegining, processedPosLemmaWords) {

    let firstWord = words[index].word;
    let namedEntity = firstWord.text;
    let namedEntityLemma = firstWord.lemma;


    if( !sentenceBegining || (sentenceBegining && (firstWord.pos == 'NOUN' || firstWord.pos == 'PROPN' || firstWord.pos == 'X' || firstWord.pos == 'ADJ')) ){

        if( index + 1 < words.length ){

            let secondWord = words[ index+1 ].word;

            if( string.initialIsCapital( secondWord.text )){ // TODO: pridat spojku napr. 'pri'

                secondWord = await nlpParser.getPosAndLemma(secondWord.text, processedPosLemmaWords);

                namedEntity = namedEntity + ' ' + secondWord.text;
                namedEntityLemma = namedEntityLemma + ' ' + secondWord.lemma;

                if( index + 2 < words.length ){
                    let thirdWord = words[ index+2 ].word;

                    if( string.initialIsCapital( thirdWord.text )){
                        thirdWord = await nlpParser.getPosAndLemma(thirdWord.text, processedPosLemmaWords);

                        namedEntity = namedEntity + ' ' + thirdWord.text;
                        namedEntityLemma = namedEntityLemma + ' ' + thirdWord.lemma;
                    }
                }
            }
            else if( string.isSimilar( secondWord.text, 'jazero', 0.85 ) || string.isSimilar( secondWord.text, 'tajch', 0.85 )){ // TODO: pridat viacej slov

                secondWord = await nlpParser.getPosAndLemma(secondWord.text, processedPosLemmaWords);

                namedEntity = namedEntity + ' ' + secondWord.text;
                namedEntityLemma = namedEntityLemma + ' ' + secondWord.lemma;
            }
            else if( sentenceBegining){
                if( firstWord.pos == 'NOUN' || firstWord.pos == 'PROPN' ) return { word: namedEntity, lemma: namedEntityLemma, relevancy: 'potential'};
                else return { word: '', relevancy: ''};
            }
        }

        return { word: namedEntity, lemma: namedEntityLemma, relevancy: 'named-entity'};

    }

    return { word : '', relevancy: '' };


}

export function countNamedEntities( namedEntities ){
    let countedNameEntities = [];

    for (let nameEntity of namedEntities) {

        nameEntity = addNamedEntity( countedNameEntities, nameEntity );
        if(nameEntity != null) countedNameEntities.push( nameEntity );
    }

    return countedNameEntities;

}

export function addNamedEntity( countedNameEntities, nameEntity ){

    let countedNameEntity = array.isLemmaInArray( countedNameEntities, nameEntity );

    if( countedNameEntity == null ) return nameEntity;
    else{
        countedNameEntity.relevancy += nameEntity.relevancy;

        countedNameEntity.word = setNamedEntityName( countedNameEntity.word, nameEntity.word );
    }

    return null;

}

export function setNamedEntityName( firstName, secondName){
    if( secondName.length < firstName.length ) return secondName;
    else return firstName;
}

export function groupEntitiesBySimilarity( namedEntities ){
    let groupedNameEntities = [];

    for (let nameEntity of namedEntities) {

        let groupedNameEntity = array.isSimilarInArray( groupedNameEntities, nameEntity );

        if( groupedNameEntity == null ) groupedNameEntities.push( nameEntity );
        else{
            groupedNameEntity.relevancy += nameEntity.relevancy;

            groupedNameEntity.word = setNamedEntityName( groupedNameEntity.word, nameEntity.word );
        }

    }

    return groupedNameEntities;

}

export function addRelevantPotentialEntities( namedEntities, potentialNamedEntities ){
    let newNamedEntities = [];
    for( let potentialNamedEntity of potentialNamedEntities){
        for( let namedEntity of namedEntities){
            let similarity = string.similarityOfStrings( potentialNamedEntity.word, namedEntity.word);
            if( similarity > 0.6 ){

                let newNamedEntity = addNamedEntity(namedEntities, potentialNamedEntity);
                if(newNamedEntity != null) newNamedEntities.push( newNamedEntity );

                console.log('--addRelevantPotentialEntities: ' + potentialNamedEntity.word);
                break;
            }
        }
    }
    return namedEntities.concat(newNamedEntities);
}