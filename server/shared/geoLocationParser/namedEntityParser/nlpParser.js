import axios from "axios";
import * as array from '../_lib/array';

export function getWords( inputString ){

    let sentences = inputString.split(/(?=[.!?,:()])|(?<=[.!?,:()])/g);
    let wordObjects = [];

    for( let sentence of sentences ){
        let wordObject = { };

        if( sentence == '.' || sentence == '!' || sentence == '?' ){
            wordObject = { word : { text : sentence, pos : 'PUNCT' } };
            wordObjects.push( wordObject );
        }
        else{
            let words = sentence.split(' ');

            for( let word of words){
                if(word != '' && word != undefined ) {
                    wordObject = {word: {text: word}};
                    wordObjects.push(wordObject);
                }
            }
        }
    }

    return wordObjects;
}

export async function getPosAndLemma(inputString, processedPosLemmaWords ){

    let wordObject = { word : { text : inputString } };

    wordObject = array.isInArrayNestedField( processedPosLemmaWords, wordObject, 'word', 'text' );

    if( wordObject == null ) {

        if (inputString.length > 2) {

            let res = await axios.post( "http://localhost:3008/api/words", [{sentence: inputString}] );

            processedPosLemmaWords.push( res.data[0] );
            return res.data[0].word;

        } else {
            wordObject = { word: {text: inputString, lemma: inputString.toLowerCase(), pos: 'ADP'} };
            processedPosLemmaWords.push( wordObject );

            return wordObject.word;
        }
    }
    else return wordObject.word;


}
