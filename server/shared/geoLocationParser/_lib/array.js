import * as string from './string';


export function orderByRelevancy( array ){

    array.sort( compareRelevancy );

    return array;
}

function compareRelevancy( a, b ) {
    if ( a.relevancy > b.relevancy ){
        return -1;
    }
    if ( a.relevancy < b.relevancy ){
        return 1;
    }
    return 0;
}

export function isInArray(array, objectToFind, fieldName) {
    for (let arrayObject of array ) {
        if( arrayObject[ fieldName ] == objectToFind[ fieldName ]) return arrayObject;
    }

    return null;
}

export function isInArrayNestedField(array, objectToFind, fieldName1, fieldName2) {

    for (let arrayObject of array ) {

        if( arrayObject[fieldName1][fieldName2] == objectToFind[fieldName1][fieldName2]) return arrayObject;

    }

    return null;
}

export function isSimilarInArray(countedNameEntities, namedEntity) {
    for (let countedNameEntity of countedNameEntities ) {
        let wordSimilarity = string.similarityOfStrings( countedNameEntity.word, namedEntity.word);
        let lemmaSimilarity = string.similarityOfStrings( countedNameEntity.lemma, namedEntity.lemma);

        if( (wordSimilarity >= 0.8) || (lemmaSimilarity >= 0.8)) return countedNameEntity;  // niekedy je lemma rozdielna ale word rovnake (ako keby bug v lemma parseri)
    }

    return null;
}

export function isLemmaInArray(countedNameEntities, nameEntity) {
    for (let countedNameEntity of countedNameEntities ) {
        if( (countedNameEntity.lemma == nameEntity.lemma) || (countedNameEntity.word == nameEntity.word)) return countedNameEntity;  // niekedy je lemma rozdielna ale word rovnake (ako keby bug v lemma parseri)
    }

    return null;
}

export function removeUndefinedItems( namedEntities ){
    let NEWithoutUndefined = [];

    for( let namedEntity of namedEntities){
        if( namedEntity != null ) NEWithoutUndefined.push(namedEntity);
    }

    return NEWithoutUndefined;
}

export function removeValueFromArray(arr, value){
    return arr.filter(item => item !== value);
}