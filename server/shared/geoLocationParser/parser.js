import NamedEntityParser from "./namedEntityParser/namedEntityParser";
import GeoLocationParser from "./geoLocationParser/geoLocationParser";
import * as postCategoryParser from "./postCategoryParser/postCategoryParser";
import Mercury from "@postlight/mercury-parser";
import "babel-polyfill";
import { performance } from 'perf_hooks';

export async function parse( postURL ){

    let startTime = performance.now();

    let articleData = await Mercury.parse(postURL);

    let namedEntityParser = new NamedEntityParser();
    let namedEntities = await namedEntityParser.getNamedEntities( articleData );

    let geoLocationParser = new GeoLocationParser();
    let mostRelevantNamedEntities = await geoLocationParser.getGeoEntities( namedEntities );

    articleData.locations = mostRelevantNamedEntities;

    articleData.category = postCategoryParser.getPostCategory( geoLocationParser.getNamedEntities(), namedEntityParser.getWords() );

    let endTime = performance.now();
    console.log(`Time ${(endTime - startTime) / 1000}s`);

    return articleData;

}