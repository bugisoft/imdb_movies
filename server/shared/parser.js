export function removeTailWhitespace(string){
    string = string.replace(new RegExp('[ \\t]+$', 'gi'), '');
    string = string.replace(new RegExp('^[ \\t]+', 'gi'), '');

    return string;
}