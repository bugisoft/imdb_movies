import express from 'express';
import "babel-polyfill";

import * as geoLocationParser from '../shared/geoLocationParser/parser';

var router = express.Router();


router.post('/', function(req, res){

    let postURL = req.body.postURL;

    geoLocationParser.parse( postURL )
        .then( articleData => {

            res.json(articleData)

        })
        .catch(  error => {

            console.log(error);

            if (typeof error === 'string' || error instanceof String) res.status(400).send( error );
            else res.status(400).send( 'error' );

        });

});




module.exports = router;