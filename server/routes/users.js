import express from 'express';
import commonValidations from '../shared/validations/signup'
import bcrypt from 'bcrypt';
import Users from '../models/users';
/*import Promise from 'bluebird';*/
import isEmpty from 'lodash/isEmpty';

var router = express.Router();

let checkEmail = function(data, errors){
        return new Promise(function(resolve, reject){
            Users.aggregate([{ $match: { 'email' : data.email}}], function (err, identicalDoc){
                    try{
                        if (err){
                            throw new Error(err);
                        }
                        else{    
                            if(identicalDoc.length > 0){
                                errors.email = 'There is user with such email';
                                reject(); 
                            }
                            else resolve();
                        }
                    }
                    catch(err){
                        errors.error = err;
                        reject();
                    }
                }
            );
        });
    }

    let checkUsername = function(data, errors){
        return new Promise(function(resolve, reject){
            Users.aggregate([{ $match: { 'username' : data.username}}], function (err, identicalDoc){
                    try{
                        if (err){
                            throw new Error(err);
                        }
                        else{
                            if(identicalDoc.length > 0){
                                errors.username = 'There is user with such username';
                                reject();
                            }
                            else resolve();
                        }
                    }
                    catch(err){
                        errors.error = err;
                        reject();
                    }
                }
            );
        });
    }

let validateInput = function(data, otherValidations){
    var { errors } = otherValidations(data);
    var Users = require('../../models/users.js');

    
    
    return new Promise(function(resolve, reject){
        Promise.all([checkEmail(data, errors), checkUsername(data, errors)]).then(function(){
            resolve({errors, isValid: isEmpty(errors)});
        }).catch(function(){
            resolve({errors, isValid: isEmpty(errors)});
        }); 
    });
}

router.post('/', function(req, res){
    validateInput(req.body, commonValidations).then(function(resolve, reject){
        const { errors, isValid } = resolve;

        if(isValid){
            const { username, email, password } = req.body;
            const passwordDigest = bcrypt.hashSync(password, 10);

            let userModel = new Users(); 
            userModel.username = username;
            userModel.email = email;
            userModel.password_digest = passwordDigest;

            userModel.save(function (err) {
                if (err) res.status(500).json({error: err});
                res.json({ success: true });
            }); 
        }
        else{
            res.status(400).json(errors);
        }
        
    });
    
    
});

router.get('/:identifier', (req, res) => {
    var errors = {};
    var data = {
        username: req.params.identifier,
        email: req.params.identifier
    };
    console.log(req.params.identifier);
    Promise.all([checkEmail(data, errors), checkUsername(data, errors)]).then(function(){
            console.log('tu66');
            res.json({errors, isValid: isEmpty(errors)});
        }).catch(function(){
            console.log('tu55');
            console.log(errors);
            res.json({errors, isValid: isEmpty(errors)});
        }); 
});
module.exports = router;