app.get('/import-test-data', function(req, res){
    var postModel = new Posts(); 
        postModel.title = "Maslo";
        postModel.postDate = new Date('2014-01-22T14:56:59.301Z');
        postModel.link = "http://marekstasak.blog.sme.sk/c/247695/Bratislava-Kittsee.html",
        postModel.domain = "sme.sk",
        postModel.place = "Bratislava",
        postModel.country = "Slovenská republika",
        postModel.location = { coordinates: [18.1121373, 50.1528923], type: "Point"}; 
        postModel.imageCropped = "1037.jpg";
        postModel.category = "hiking";
        postModel.lang = "sk";
        postModel.save(function (err) {
          if (err)
            res.send(err);

          res.json({});
        });

});

app.get('/import-old-db', function(req, res){
    /*Posts.find(function(err, posts){
        console.log('GET POSTS');
        if(err){
            console.log('# API GET BOOKS: ', err);
        }

        let post;
        for (post of posts) {

            // location
            let my_visit = new Posts({
                location: { coordinates: [post.geo2, post.geo1], type: "Point"}
            });

            let upsertData = my_visit.toObject();
            
            delete upsertData._id;

            
            Posts.update({ _id: post._id }, upsertData, { multi: false }, function(err) { if(err) { throw err; }});
            // location end

            // Date
            postDate = post.postDate2.toString();
            
            let date = null;
            //if(postDate)
            if (postDate.substring(0,1) == '0') date = '2012-09-01'+ 'T' + postDate.substring(11,19) + '.301Z';
            else date = postDate.substring(0,10) + 'T' + postDate.substring(11,19) + '.301Z';

            my_visit = new Posts({
                postDate: new Date(date),
                creationDate: new Date(date)
            });

            upsertData = my_visit.toObject();
            
            delete upsertData._id;

            Posts.update({ _id: post._id }, upsertData, { multi: false }, function(err) { if(err) { throw err; }});
            // Date End

            // domain
            my_visit = new Posts({
                domain: extractHostname(post.link)
            });

            upsertData = my_visit.toObject();
            
            delete upsertData._id;

            Posts.update({ _id: post._id }, upsertData, { multi: false }, function(err) { if(err) { throw err; }});
            //domain end
        }

        

        res.json("ok")
    })*/

    Posts.collection.update({},
      {$unset: {postDate2: true}},
      {multi: true, safe: true}
    );
   

    res.json("OK")

});