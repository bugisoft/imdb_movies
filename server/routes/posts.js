import express from 'express';
import Posts from '../models/posts';
import PostsDeleted from '../models/postsDeleted';
import {saveImage} from '../../client/utilities/imageProcessor';
import {deleteFile} from '../../client/utilities/file';

//import {runParser} from "../shared/geoLocationParser/namedEntityParser";

var router = express.Router();

function isPostInDB(postURL, res, callback){

    Posts.aggregate(
        [
            { 
                $match: { 
                    'link' : postURL
                }
            }
        ], function (err, identicalPost){
            try{

                if (err){
                    throw new Error(err);
                }
                else{
                    
                    if(identicalPost.length > 0) callback(true, res); 
                    else callback(false, res);

                }
            }
            catch(err){
                console.log(err);
                res.status(500).send(err)
            }
        
        }
    );
}

router.post('/', function(req, res){

    //runParser('Jebel Toubkal – 4167 metrov nad morom, najvyšší vrch Maroka v pohorí Atlas, najvyšší bod severnej Afriky aj celého arabského sveta.');

    var testArray = [];

    //testArray.push('word': 'toto', 'attribute_big': '2', 'attribute_small': '1');

    console.log(testArray);

    console.log('get posts');
    var reqContent = req.body;
    //console.log(reqContent);

    if(reqContent.postType == 'travel-article'){
        //console.log('travel articles1');
        var loc = reqContent.location;
        let query;
        
        if(loc.sw[0] > loc.ne[0]){
            query = {$or:[
                { "location": { "$geoWithin": { "$box": [ [ -180, loc.sw[1] ], [ loc.ne[0], loc.ne[1] ] ]  } }},
                { "location": { "$geoWithin": { "$box": [ [ loc.sw[0], loc.sw[1] ], [ 180, loc.ne[1] ] ]  } }}
            ]} 
        }
        else query = { "location": { "$geoWithin": { "$box": [ [ loc.sw[0], loc.sw[1] ], [ loc.ne[0], loc.ne[1] ] ]  } }};

        console.log('travel articles2');

        Posts.find( query, function(err, posts){
            if(err){
                console.log('# API GET POSTS: ', err);
            }
            
            console.log('Done');
            //console.log(posts);
            res.json(posts)
        }).sort({postDate: -1 }).limit(30);

        /*Posts.findOne({}, function(err, result) {
            if (err) throw err;
            console.log(result);
            //db.close();
        });*/

        /*Posts.find( {}, function(err, posts){
            if(err){
                console.log('# API GET POSTS: ', err);
            }

            console.log('Done');
            console.log(posts);
            res.json(posts)
        });*/
    }
    if(reqContent.postType == 'traveller'){
        console.log('traveller');
        var loc = reqContent.location;
        var travellerID = reqContent.travellerID.replace(new RegExp('-', 'g'), '.');
        let query;
        if(loc.sw[0] > loc.ne[0]){
            query = {$or:[
                { "location": { "$geoWithin": { "$box": [ [ -180, loc.sw[1] ], [ loc.ne[0], loc.ne[1] ] ]  } }},
                { "location": { "$geoWithin": { "$box": [ [ loc.sw[0], loc.sw[1] ], [ 180, loc.ne[1] ] ]  } }}
            ]} 
        }
        else query = { "location": { "$geoWithin": { "$box": [ [ loc.sw[0], loc.sw[1] ], [ loc.ne[0], loc.ne[1] ] ]  } }};
        console.log('traveller: ' + reqContent.travellerID);

        Posts.aggregate(
                    [
                        {'$match': {'domain': travellerID}},
                        { 
                            $match:  
                                query
                            
                        },
                        { $limit : 30 }          

                    ], function (err, result) {
                    if (err) {
                        next(err);
                    } else {
                        
                        //console.log(result);        
                        res.json(result);
                    }
                });

        /*Posts.find( query, function(err, posts){
            if(err){
                console.log('# API GET POSTS: ', err);
            }
            
            console.log('Done');
            res.json(posts)
        }).limit(30);*/
    }
    if(reqContent.postType == 'travellers'){

        let smeBlogs = null;

        Posts.aggregate(
            [
                { $match : { domain : { $regex: 'blog.sme.sk', $options: 'g' } } },
                {'$match': {'domain': {'$ne': 'valentova.blog.sme.sk'}}},
                {$sort: {"creationDate":1}},
                { 
                    "$group": {
                        "_id": "$domain",
                        "imageCropped": { "$first": "$imageCropped" } ,
                        "link": { "$first": "$link" } ,
                        "domain": { "$first": "$domain" },
                        "postDate": {"$last": "$postDate"},
                        "count": { "$sum": 1}
                    } 
                },
                {$sort: {"count":-1}},
                { $limit : 30 }

            ], function (err, smeBlogs) {
            if (err) {
                next(err);
            } else {
                
                Posts.aggregate(
                    [
                        { 
                            $match: { 
                                'creationDate' : {
                                    $gt: new Date("2013-11-10T14:56:59.301Z")
                                }
                            }
                        },
                        { 
                            "$group": {
                                "_id": "$domain",
                                "imageCropped": { "$first": "$imageCropped" } ,
                                "link": { "$first": "$link" } ,
                                "domain": { "$first": "$domain" },
                                "postDate": {"$last": "$postDate"},
                                "count": { "$sum": 1}
                            } 
                        } 
                

                    ], function (err, newestPosts) {
                    if (err) {
                        next(err);
                    } else {         
                        res.json([...newestPosts, ...smeBlogs]);
                    }
                });
            }
        });

        
    }
});

router.post('/post', function(req, res){  //toto by asi malo byt app.put()
    var post = req.body;
    
    let imageCropDimmensions = post[0].imageCropped;

    let frontendImage = post[0].image;

    post[0].imageCropped = "";
    //post[0].image = "";

    isPostInDB(post[0].link, res, function(req, res){
        if(req == true) res.json({ error : 'Ďakujeme, ale tento článok sa už nachádza na stránke!' });
        if(req == false){
            Posts.create(post, function(err, savedPost){

                if(err){
                    console.log(err);
                    res.status(500).send(err)
                }
            
                let destImage = savedPost[0]._id + '.jpg';
                try{
                    saveImage(frontendImage, destImage, imageCropDimmensions, savedPost[0]._id, function(){
                        res.json(savedPost);
                    }); 
                }
                catch(err){
                    console.log(err);
                    res.status(500).send(err)
                }
                
            })

        }
    });

});

router.post('/getPost', function(req, res){  
    console.log('som tu');
    let postURL = req.body.postURL;
    
    Posts.aggregate(
        [
            { 
                $match: { 
                    'link' : postURL
                }
            }
        ], function (err, post){
            try{

                if (err){
                    throw new Error(err);
                }
                else{
                    console.log(post);
                    res.json(post);
                }
            }
            catch(err){
                console.log(err);
                res.status(500).send(err)
            }
        
        }
    );
        
       
});

router.post('/getPostByID', function(req, res){  
    console.log('som tu');
    var postID = mongoose.Types.ObjectId(req.body.postID);
    console.log(postID);
    Posts.aggregate(
        [
            { 
                $match: { 
                    '_id' : postID
                }
            }
        ], function (err, post){
            try{

                if (err){
                    throw new Error(err);
                }
                else{
                    console.log(post);
                    res.json(post);
                }
            }
            catch(err){
                console.log(err);
                res.status(500).send(err)
            }
        
        }
    );
        
       
});

router.post('/postUpdate', function(req, res){
    console.log('*********************');
    console.log(req.body[0]);
    var reqPost = req.body[0];

    Posts.aggregate(
        [
            { 
                $match: { 
                    '_id' : mongoose.Types.ObjectId(reqPost.editPostID)
                }
            }
        ], function (err, post){
            try{

                if (err){
                    throw new Error(err);
                }
                else{
                    console.log(reqPost.title);
                    console.log(post);
                    console.log(post[0]._id);

                    let newPostData = new Posts({
                        title : reqPost.title,
                        postDate : reqPost.postDate
                    });
                    
                    let newPostDataObject = newPostData.toObject();
                    delete newPostDataObject._id;

                    Posts.update({ _id: post[0]._id }, newPostDataObject, { multi: false }, function(err) {
                        if(err) { throw err; }

                        if(post[0].imageCropped != undefined || post[0].imageCropped != ''){
                            deleteFile('/public/assets/img/results/' + post[0].imageCropped, function(){

                                let destImage = post[0]._id + '.jpg';
                                let imageCropDimmensions = reqPost.imageCropped;

                                saveImage(reqPost.image, destImage, imageCropDimmensions, post[0]._id, function(){
                                    res.json('ok');
                                });
                            });
                        }
                    });
                }
            }
            catch(err){
                console.log(err);
                res.status(500).send(err)
            }
        
        }
    );

});

router.post('/deletePost', function(req, res){  
    
    var postID = req.body.postID;
    
    Posts.findById(postID, function (err, post){

        var deletedPost = new PostsDeleted();
            
        deletedPost.title = post.title;
        deletedPost.postDate = post.postDate;
        deletedPost.link = post.link;
        deletedPost.domain = post.domain;
        deletedPost.place = post.place;
        deletedPost.country = post.country;
        deletedPost.location = post.location;
        deletedPost.image = post.image; 
        deletedPost.imageCropped = post.imageCropped;
        deletedPost.category = post.category;
        deletedPost.creationDate = post.creationDate;
        deletedPost.lang = post.lang;

        deletedPost.save(function (err) {
            if (err) res.status(500).send(err)
            else{

                deleteFile(__dirname + '/../public/assets/img/results/' + deletedPost.imageCropped, function(){

                    Posts.remove({ _id: postID }, function(err) {
                        if (err) res.status(500).send(err)
                        else {
                            res.json({ response : 'ok' }); 
                            console.log('Post Deleted');
                        }
                    });
                }); 
            }
        });
    });      
});

router.post('/isPostInDB', function(req, res){  
    
    let postURL = req.body.postURL;
    
    isPostInDB(postURL, res, function(req, res){
        console.log('isPostInDB' + req);
        if(req == true) res.json({ response : true }); 
        if(req == false) res.json({ response : false });
    });
       
});

module.exports = router;