import express from 'express';
import bcrypt from 'bcrypt';
import Users from '../models/users';
import jwt from 'jsonwebtoken';
import config from '../config';

var router = express.Router();


router.post('/', function(req, res){
    const { identifier, password } = req.body;
    
    Users.findOne( {$or:[
                { "username": identifier},
                { "email": identifier}
            ]}, function(err, user){
                if(err){
                    console.log('# API GET POSTS: ', err);
                }
                
                if(user != null){
                    if(bcrypt.compareSync(password, user.password_digest)){
                        const token = jwt.sign({
                            id: user._id,
                            username: user.username
                        }, config.jwtSecret);
                        res.json({token});
                    }
                    else res.status(401).json({ errors: { form: 'Invalid credentials.' }});
                }
                else res.status(401).json({ errors: { form: 'Invalid credentials.' }});
        })
    
});


module.exports = router;