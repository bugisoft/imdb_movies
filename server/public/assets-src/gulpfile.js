var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass')(require('sass'));
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var replace = require('gulp-replace');
var merge = require('merge-stream');
var browserSync = require('browser-sync').create();

var plumberErrorHandler = { errorHandler: notify.onError({
 
    title: 'Gulp',
 
    message: 'Error: <%= error.message %>'
 
  })
 
};


gulp.task('sass', function () {
    var sassStream,
        cssStream;

    sassStream = gulp.src('./css/*.scss')
        .pipe(plumber(plumberErrorHandler))
        .pipe(sass())
        .pipe(gulp.dest('../assets/css'))
        .pipe(browserSync.stream());
 
});



gulp.task('concatVendorsCss', function () {
  gulp.src(['./libs/fancybox/source/jquery.fancybox.css',
            './libs/flexslider-destroy/flexslider.css'])
      .pipe(concat('vendors.css'))
      .pipe(gulp.dest('../assets/css'));
});

gulp.task('js', function () {
 
gulp.src(['./js/components/*.js', './js/pages/*.js', './js/main.js'])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('../assets/js'));
});

gulp.task('replaceCssAssetsPath', function() {
  gulp.src('./libs/fancybox/source/jquery.fancybox.css')
      .pipe(replace("url('", "url('./vendors/img/fancybox"))
      .pipe(gulp.dest('./libs/fancybox/source'));

  gulp.src('./libs/flexslider-destroy/flexslider.css')
      .pipe(replace("url(images", "url('./vendors/img/flexslider"))
      .pipe(gulp.dest('./libs/flexslider-destroy'));
});

gulp.task('copyCssAssets', function() {
    gulp.src(['./libs/fancybox/source/blank.gif', 
            './libs/fancybox/source/fancybox_loading.gif',
            './libs/fancybox/source/fancybox_loading@2x.gif',
            './libs/fancybox/source/fancybox_overlay.png',
            './libs/fancybox/source/blank.gif',
            './libs/fancybox/source/fancybox_sprite.png',
            './libs/fancybox/source/fancybox_sprite@2x.png'])
        .pipe(gulp.dest('../assets/css/vendors/img/fancybox'));

    gulp.src(['./libs/flexslider-destroy/images/**'])
        .pipe(gulp.dest('../assets/css/vendors/img/flexslider'));

    gulp.src('./libs/bootstrap-sass/assets/fonts/**')
        .pipe(gulp.dest('../assets/css/vendors/fonts/'));
      
});

gulp.task('copyVendorLibs', function() {
    gulp.src(['./libs/font-awesome/css/**', 
            './libs/font-awesome/fonts/**'], {base: './libs/font-awesome/'})
        .pipe(gulp.dest('../assets/libs/font-awesome'));

    gulp.src('./libs/bootstrap-sass/assets/javascripts/bootstrap.min.js')
        .pipe(gulp.dest('../assets/libs/bootstrap-sass'));

    gulp.src('./libs/waitForImages/dist/jquery.waitforimages.min.js')
        .pipe(gulp.dest('../assets/libs/waitForImages'));
});

gulp.task('watch', function() {

  //browserSync.init({
  //  proxy: 'http://localhost/Dropbox/zahumnami/public/'
  //});
 
  gulp.watch(['./css/*.scss', './css/**/*.scss'], ['sass']);
 
  //gulp.watch(['./js/*.js', './js/pages/*.js', './js/components/*.js'], ['js']);

  //gulp.watch(['../*.php', '../includes/*.php', '../page-templates/*.php', '../bs-post-category-list/*.php', '../woocommerce/*.php'])
    //  .on('change', browserSync.reload);
});

function gulpWatch() {
    return gulp
        .watch(['./css/*.scss', './css/**/*.scss'], gulp.series('sass'));
}
gulp.task(gulpWatch);

gulp.task('gulpWatch', function() {
    gulp.watch(['./css/*.scss', './css/**/*.scss'], gulp.series('sass'));
    //done();
});


//gulp.task('default', gulp.series('gulpSass', 'gulpWatch'));


function gulpSass() {
    return gulp
        .src('./css/*.scss')
        .pipe(plumber(plumberErrorHandler))
        .pipe(sass())
        .pipe(gulp.dest('../assets/css'))
        .pipe(browserSync.stream());
}
gulp.task(gulpSass);


gulp.task('sass', function() {
    return gulp.src('./css/*.scss')
        .pipe(sass.sync({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(gulp.dest('../assets/css'))
        .pipe(browserSync.stream());
});

// Static Server and watching scss/html files
gulp.task('server', function (done) {
    browserSync.init({
        server: {
            baseDir: "http://localhost:3002/"
        }
    });
    gulp.watch(['./css/*.scss', './css/**/*.scss'], gulp.series('sass'));

    //gulp.watch("template/assets/scss/**/*.scss", gulp.series('sass'));
    // gulp.watch("template/assets/scss/**/*.scss", browserSync.reload);
    gulp.watch("template/**/*.html", browserSync.reload);
    done();
});


// gulp.task('watch', function() {
//    gulp.watch(['sass']);
// });

gulp.task('default', gulp.series('sass', 'server'));
