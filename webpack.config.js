var path = require('path');

module.exports = {
    entry: './client/client.js',
    output: {
        filename: 'assets/js/bundle.js',
        path: path.resolve(__dirname, 'server/public')
    },
    watch: true,
    module:{
        rules: [
            {
                test:/\.js$/,
                exclude:/node_modules/,
                loader: 'babel-loader',
                query:{
                    presets: ['react', 'es2015', 'stage-1']
                }
            }
        ]
    },
    node: {
        net: 'empty',
        dns: 'empty'
    }
}